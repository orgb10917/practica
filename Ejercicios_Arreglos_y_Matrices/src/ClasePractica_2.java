import java.util.Scanner;

public class ClasePractica_2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int [][]matriz = new int[3][4];
        int [][]cambio = new int[3][4];
        int suma = 0;
        float promedio;

        for(int i=0; i<matriz.length; i++)
        {
            for (int j=0; j<matriz[i].length;j++)
            {
                System.out.println("Ingrese el elemento ["+i+"] ["+j+"]");
                matriz[i][j]=sc.nextInt();
            }
        }

        //Mostramos matriz original
        System.out.println(">>>>Matriz original<<<<");
        for (int i=0; i<matriz.length; i++)
        {
            for (int j=0; j<matriz[i].length; j++)
            {
                System.out.print(matriz[i][j]+"\t");
            }
            System.out.println();
        }

        //Copiamos la matriz original a una segunda matriz
        for (int i=0; i<matriz.length; i++)
        {
            for (int j=0; j<matriz[i].length; j++)
            {
                cambio [i] [j] = matriz [i] [j];
                suma += matriz [i] [j]; // sumamos cada uno de los valores de la matriz
            }
            System.out.println();
        }

        promedio = (float) suma / 12; // Calculamos promedio

        //Intercambiamos la fila 1 de la matriz original con la fila 2 de la copia
        for (int i=0; i<matriz[0].length; i++)
        {
            cambio [1] [i] = matriz [0] [i];
        }

        //Intercambiamos la fila 2 de la matriz original con la fila 1 de la copia
        for (int i=0; i<matriz[1].length; i++)
        {
            cambio [0] [i] = matriz [1] [i];
        }

        //Imprimimos
        System.out.println(">>>>Nueva matriz<<<<");

        for (int i=0; i<matriz.length; i++)
        {
            for (int j=0; j<matriz[i].length; j++)
            {
                System.out.print(cambio[i][j]+"\t");
            }
            System.out.println();
        }
        System.out.println("Promedio = "+promedio);
    }
}