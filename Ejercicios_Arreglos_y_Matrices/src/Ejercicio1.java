/*
Ejercicio 1:
Calcular la media de una serie de números que se leen por teclado
 */
import java.util.Scanner;

public class Ejercicio1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n;
        int suma=0;
        float media;

        System.out.println("Ingrese la cantidad de números que va a digitar: ");
        n = sc.nextInt();

        int [] numeros = new int[n];

        for (int i = 0; i<numeros.length; i++){
            System.out.println("Digite el número: "+(i+1));
            numeros[i] = sc.nextInt();
        }

        for (int numero : numeros) {
            suma += numero;
        }

        media = (float)(suma / n);

        System.out.println("La media es: "+media);
    }
}
