/*
Ejercicio 2:
Programa Java que lea por teclado 10 números enteros y los guarde en un array.
A continuación calcula y muestra por separado la media de los valores positivos y
la de los valores negativos.
 */
import java.util.Scanner;

public class Ejercicio2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int numeros_negativo = 0;
        int numeros_positivos = 0;
        int suma_positiva=0;
        int suma_negativa =0;
        float media_negativa;
        float media_positiva;
        int [] numeros = new int[10];

        for (int i = 0; i<numeros.length; i++){
            System.out.println("Digite el número "+(i+1)+" : ");
            numeros[i] = sc.nextInt();
        }

        for (int i = 0; i<numeros.length; i++){
            if (numeros[i]<0){
                suma_negativa += numeros[i];
                numeros_negativo = numeros_negativo + 1;
            }else if (numeros[i]>0){
                suma_positiva += numeros[i];
                numeros_positivos = numeros_positivos +1;
            }
        }

        media_negativa = (float) (suma_negativa / numeros_negativo);
        media_positiva = (float) (suma_positiva / numeros_positivos);


        System.out.println("Media de los números positivos =  " + media_positiva);
        System.out.println("Media de los números negativos =  " + media_negativa);

    }
}
