/*
Ejercicio 4:
Leer N alturas y calcular la altura media. Calcular cuántas hay superiores a la
media y cuántas inferiores
 */
import java.util.Scanner;

public class Ejercicio4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int contador_alturas_inferiores = 0;
        int contador_alturas_superiores = 0;
        int suma = 0;
        float promedio;
        int cantidad_alturas;

        System.out.println("Ingrese la cantidad de alturas que desea ingresar: ");
        cantidad_alturas = sc.nextInt();

        int [] alturas = new int[cantidad_alturas];

        for (int i = 0; i<= alturas.length; i++){
            System.out.println("Ingrese la altura número "+(i+1)+": ");
            alturas[i] = sc.nextInt();
            suma += alturas[i];
        }

        promedio = (float)(suma/cantidad_alturas);

        for (int i = 0; i<= alturas.length; i++){
            if (alturas[i]<promedio){
                contador_alturas_inferiores = contador_alturas_superiores + 1;
            }else{
                contador_alturas_superiores = contador_alturas_superiores + 1;
            }
        }

        System.out.println("La altura media es de: "+promedio);
        System.out.println("La cantidad de alturas inferiores a la media es de: "+contador_alturas_inferiores);
        System.out.println("La cantidad de alturas superiores a la media es de: "+contador_alturas_superiores);
    }
}
