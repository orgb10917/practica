/*
Ejercicio 8:
Con base en una encuesta realizada a N estudiantes (N<=10) en una Universidad
donde se solicitó la siguiente información: CÉDULA, SEXO, SUELDO, TRABAJA.
Donde:
    - CEDULA (Es un número entero)
    - SEXO (1 - Masculino 2 – Femenino)
    - TRABAJA (1 - Si trabaja 2 - No trabaja)
    - SUELDO (Es un valor entero)
Escriba el programa que permita leer y almacenar en vectores los datos solicitados, luego,
calcular e imprimir:
    a) Porcentaje de hombres en la Universidad
    b) Porcentaje de mujeres en la Universidad
    c) Porcentaje de hombres que trabajan y sueldo promedio
    d) Porcentaje de mujeres que trabajan y sueldo promedio
 */
import java.util.Scanner;

public class Ejercicio8 {
    public static void main(String[] args) {
        Scanner sc  = new Scanner(System.in);

        int i = 0;
        int [][] encuesta = new int[10][4];
        int opc;
        boolean flag = true;

        do {
            System.out.println("Ingrese una opción: ");
            System.out.println("1. Ingresar un nuevo estudiante en la encuesta ");
            System.out.println("2. Mostrar el registro de encuestados");
            System.out.println("3. Mostrar resultados");
            System.out.println("4. Salir");
            opc = sc.nextInt();

            switch (opc){
                case 1:
                    System.out.println("Encuestado número:  "+(i+1));
                    for (int j = 0; j<encuesta[i].length; j++){
                        if (j==0){
                            System.out.println("Cédula: ");
                            encuesta[i][j] = sc.nextInt();
                        }
                        if (j==1){
                            do {
                                System.out.println("Sexo, digite: ");
                                System.out.println("1. Masculino");
                                System.out.println("2. Femenino");
                                encuesta [i][j] = sc.nextInt();
                            }while (encuesta[i][j] <1 || encuesta[i][j]>2);
                        }
                        if (j==2){
                            do {
                                System.out.println("Trabaja, digite: ");
                                System.out.println("1. Si");
                                System.out.println("2. No");
                                encuesta [i][j] = sc.nextInt();
                            }while (encuesta[i][j] <1 || encuesta[i][j]>2);
                        }
                        if (j==3){
                            if (encuesta[i][2] == 1){
                                System.out.println("Ingrese el sueldo que recibe: ");
                                encuesta [i][j] = sc.nextInt();
                            }else{
                                encuesta [i][j] = 0;
                            }
                        }
                    }
                    i++;
                    break;

                case 2:
                    System.out.println(">>>Registro de enuestados<<<");


                    break;

                case 3:
                    int numero_hombres = 0, numero_mujeres = 0;
                    float porcentaje_hombres, porcentaje_mujeres;

                    int numero_hombres_trabajan = 0, numero_mujeres_trabajan = 0;
                    float porcentaje_hombres_trabajan, porcentaje_mujeres_trabajan;

                    int suma_sueldo_hombres = 0, suma_sueldo_mujeres = 0;
                    float sueldo_promedio_hombres, sueldo_promedio_mujeres;

                    for (int k = 0; k<i; k++){
                        for (int j = 0; j<encuesta[k].length; j++){
                            if (j == 1){
                                if (encuesta[k][1] == 1){
                                    numero_hombres++;
                                }else{
                                    numero_mujeres++;
                                }
                            }
                            if (j == 2){
                                if (encuesta[k][1] == 1){
                                    if ((encuesta[k][2] == 1)){
                                        numero_hombres_trabajan++;
                                    }
                                }else{
                                    if ((encuesta[k][2] == 1)){
                                        numero_mujeres_trabajan++;
                                    }
                                }
                            }
                            if (j == 3){
                                if (encuesta[k][1] == 1){
                                    suma_sueldo_hombres += encuesta[k][j];
                                }else{
                                    suma_sueldo_mujeres += encuesta[k][j];
                                }

                            }
                        }
                    }
                    porcentaje_hombres = (float) ((numero_hombres*100)/i);
                    porcentaje_mujeres = (float) ((numero_mujeres*100)/i);
                    porcentaje_hombres_trabajan = (float) ((numero_hombres_trabajan*100)/i);
                    porcentaje_mujeres_trabajan = (float) ((numero_mujeres_trabajan*100)/i);
                    sueldo_promedio_hombres = (float) ((suma_sueldo_hombres)/numero_hombres);
                    sueldo_promedio_mujeres = (float) ((suma_sueldo_mujeres)/numero_mujeres);

                    System.out.println("Porcentaje de hombres en la Universidad es de: "+porcentaje_hombres+"%");
                    System.out.println("Porcentaje de mujeres en la Universidad es de: "+porcentaje_mujeres+"%");
                    System.out.println("Porcentaje de hombres que trabajan: "+porcentaje_hombres_trabajan+"%");
                    System.out.println("Porcentaje de mujeres que trabajan: "+porcentaje_mujeres_trabajan+"%");
                    System.out.println("Sueldo promedio de los hombres: $"+sueldo_promedio_hombres);
                    System.out.println("Sueldo promedio de las mujeres: $"+sueldo_promedio_mujeres);

                    break;

                case 4:
                    System.out.println("Gracias por utilizar la APP");
                    flag = false;
                    break;

                default:
                    System.out.println("Opción incorrecta, ingrese una opción válida");
                    break;
            }
        }while (flag);
    }
}