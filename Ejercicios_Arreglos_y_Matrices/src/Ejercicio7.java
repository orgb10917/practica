/*
Ejercicio 7:
Leer 10 números enteros, almacenarlos en un vector y determinar cuántas veces
está repetido el mayor.
 */
import java.util.Scanner;

public class Ejercicio7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int [][] numeros = new int[5][2];
        int mayor = 0;
        int contador = 0;

        System.out.println("Ingrese los elementos del vector: ");
        for(int i=0; i<numeros.length; i++)
        {
            for (int j=0; j<numeros[i].length;j++)
            {
                System.out.println("Ingrese el elemento ["+i+"] ["+j+"]");
                numeros[i][j]=sc.nextInt();
            }
        }

        System.out.println(">>>>Vector ingresado: <<<<");
        for (int i=0; i<numeros.length; i++)
        {
            for (int j=0; j<numeros[i].length; j++)
            {
                System.out.print(numeros[i][j]+"\t");
            }
            System.out.println();
        }

        for (int i=0; i<numeros.length; i++)
        {
            for (int j=0; j<numeros[i].length; j++)
            {
                if (numeros[i][j]>mayor){
                    mayor = numeros[i][j];
                    contador ++;
                }
            }
        }

        System.out.println("El número mayor es: "+mayor);
        if (contador == 1){
            System.out.println("No se repite");
        }else if (contador == 2){
            System.out.println("Se repite: "+contador+ " vez");
        }else{
            System.out.println("Se repite: "+contador+" veces");
        }
    }
}
