import java.util.Scanner;

public class ClasePractica_1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int opc;
        int j =0;
        int [] registro = new int[10];
        float media;
        int suma =0;
        boolean flag = true;

        do {
            System.out.println("Ingrese su opción: ");
            System.out.println("1. Registrar valor real");
            System.out.println("2. Visualizar los valores registrados");
            System.out.println("3. Calcular el promedio de los valores");
            System.out.println("4. Salir");
            opc = sc.nextInt();

            switch (opc){
                case 1:

                    System.out.println("Digite el valor: ");
                    registro[j] = sc.nextInt();
                    suma += registro[j];
                    j++;

                    break;
                case 2:
                    for (int i = 0; i<j; i++){
                        System.out.println("Valor: "+(i+1)+" : "+registro[i]);
                    }
                    break;
                case 3:
                    media = (float) (suma / j);
                    System.out.println("La media es: "+media);
                    break;
                case 4:
                    System.out.println("Gracias por usar la app");
                    flag = false;
                    break;
                default:
                    System.out.println("Opción incorrecta");
                    break;
            }
        }while(flag);

    }
}
