/*
Ejercicio 5:
Leer el nombre y sueldo de 20 empleados y mostrar el nombre y sueldo del
empleado que más gana.

 */
import java.util.Scanner;

public class Ejercicio5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean flag = true;
        int opc;
        int i = 0;
        String [] nombres = new String[20];
        float [] sueldos = new float[20];
        float mayor_sueldo = 0;
        String nombre_mayor_sueldo = null;

        do {
            System.out.println("Ingrese una opción: ");
            System.out.println("1. Ingresar empleado");
            System.out.println("2. Mostrar empleados ingresados");
            System.out.println("3. Mostrar empleado con mayor ganancia");
            System.out.println("4. Salir");
            opc = sc.nextInt();

            switch (opc){
                case 1:
                    sc.nextLine();
                    System.out.println("Empleado número "+(i+1));
                    System.out.println("Ingrese el nombre: ");
                    nombres[i] = sc.nextLine();
                    System.out.println("Ingrese el sueldo: ");
                    sueldos[i] = sc.nextFloat();
                    i++;
                    break;

                case 2:
                    for (int j = 0; j<i; j++){
                        System.out.println("Nombre: "+nombres[j]+" sueldo: $"+sueldos[j]);
                    }
                    break;

                case 3:
                    for (int j = 0; j < i; j++){
                        if (sueldos[j]>mayor_sueldo){
                            nombre_mayor_sueldo = nombres[j];
                            mayor_sueldo = sueldos[j];
                        }
                    }

                    System.out.println("El empleado con mayor sueldo es: "+nombre_mayor_sueldo);
                    System.out.println("Su ganancia es de: $"+mayor_sueldo);
                    break;

                case 4:
                    System.out.println("Gracias por utilizar el programa");
                    flag = false;
                    break;

                default:
                    System.out.println("Ingrese una opción correcta");
                    break;
            }

        }while (flag);
    }
}
