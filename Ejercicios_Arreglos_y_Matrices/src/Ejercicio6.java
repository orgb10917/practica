/*
Ejercicio 6:
Dado dos vectores A y B de 15 elementos cada uno, obtener un vector C donde la
posición i se almacene la suma de A[i]+B[i].
 */
import java.util.Scanner;

public class Ejercicio6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int [][] vector_A = new int[3][5];
        int [][] vector_B = new int[3][5];
        int [][] vector_C = new int[3][5];

        System.out.println("Ingrese los elementos del vector A: ");
        for(int i=0; i<vector_A.length; i++)
        {
            for (int j=0; j<vector_A[i].length;j++)
            {
                System.out.println("Ingrese el elemento ["+i+"] ["+j+"]");
                vector_A[i][j]=sc.nextInt();
            }
        }

        System.out.println("Ingrese los elementos del vector B: ");
        for(int i=0; i<vector_B.length; i++)
        {
            for (int j=0; j<vector_B[i].length;j++)
            {
                System.out.println("Ingrese el elemento ["+i+"] ["+j+"]");
                vector_B[i][j]=sc.nextInt();
            }
        }

        System.out.println(">>>>Matriz original: Vector A<<<<");
        for (int i=0; i<vector_A.length; i++)
        {
            for (int j=0; j<vector_A[i].length; j++)
            {
                System.out.print(vector_A[i][j]+"\t");
            }
            System.out.println();
        }

        System.out.println(">>>>Matriz original: Vector B<<<<");
        for (int i=0; i<vector_B.length; i++)
        {
            for (int j=0; j<vector_B[i].length; j++)
            {
                System.out.print(vector_B[i][j]+"\t");
            }
            System.out.println();
        }

        System.out.println(">>>>Nueva matriz: Vector C<<<<");
        for (int i=0; i<vector_A.length; i++)
        {
            for (int j=0; j<vector_A[i].length; j++)
            {
                vector_C [i][j] = vector_A[i][j] + vector_B[i][j]; //hacemos el cambio de valores
                System.out.print(vector_C[i][j]+"\t"); //Mandamos a imprimir
            }
            System.out.println("");
        }
    }
}