/*
Ejercicio 3:
Programa que lee por teclado la nota de los alumnos de una clase y calcula la nota
media del grupo. También muestra los alumnos con notas superiores a la media.
El número de alumnos se lee por teclado.
 */
import java.util.Scanner;

public class Ejercicio3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int cantidad_alumnos;
        int suma=0;
        float promedio;

        System.out.println("Digite la cantidad de alumnos que hay en el grupo: ");
        cantidad_alumnos = sc.nextInt();

        int[] notas = new int[cantidad_alumnos];

        for (int i = 0; i<notas.length; i++){
            System.out.println("Digite la nota del alumno número "+(i+1)+" : ");
            notas[i] = sc.nextInt();
            suma += notas[i];
        }

        promedio = (float) (suma / cantidad_alumnos);

        System.out.println("----------------------------------------------------------------");
        System.out.println("Media del grupo es de: "+promedio+" puntos.");
        for (int i = 0; i<notas.length; i++){
            if (notas[i]>=promedio){
                System.out.println("El alumno número "+(i+1)+" superó la media con: "+notas[i]+"puntos.");
            }
        }
        System.out.println("----------------------------------------------------------------");
    }
}
