import java.util.Scanner;

public class ejercicio2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int edad;

        do {
            System.out.println("¿Cuál es tu edad?");
            edad = sc.nextInt();
        }while(edad<1||edad>100);
        sc.close();
        System.out.println("----------------------------------------");
        System.out.println("Ahora se que tienes "+edad+" años");
        System.out.println("----------------------------------------");
    }
}
