import java.util.Scanner;

public class ejercicio3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int hora, minuto, segundo;

        System.out.println("formato de hora: hh/mm/ss");
        do {
            System.out.println("Digite la hora: ");
            hora = sc.nextInt();
        }while(hora<0||hora>24);

        do {
            System.out.println("Digite los minutos: ");
            minuto = sc.nextInt();
        }while(minuto<0||minuto>59);

        do {
            System.out.println("Digite los segundos: ");
            segundo = sc.nextInt();
        }while(segundo<0||segundo>59);
        sc.close();

        System.out.println("---------------------------------------------------");
        System.out.println("Hora introducida correctamente!!!");
        System.out.println("La hora es: "+hora+": "+minuto+": "+segundo);
        System.out.println("---------------------------------------------------");
    }
}
