import java.util.Scanner;

public class ejercicio4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String primera, segunda, tercera, cuarta;
        float altura;

        System.out.println("Introduce la inicial de tu primer nombre: ");
        primera = sc.next();

        System.out.println("Introduce la inicial de tu segundo nombre: ");
        segunda = sc.next();

        System.out.println("Introduce la inicial de tu primer apellido: ");
        tercera = sc.next();

        System.out.println("Introduce la inicial de tu segundo apellido: ");
        cuarta = sc.next();

        System.out.println("Introduce tu altura: ");
        altura = sc.nextFloat();
        sc.close();

        System.out.println("-----------------------------------------------------");
        System.out.println("Tus iniciales son: "+primera+"."+segunda+"."+tercera+"."+cuarta);
        System.out.println("Tienes una altura de: "+altura);
        System.out.println("-----------------------------------------------------");
    }
}
