import java.util.Scanner;

public class ejercicio1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numero;

        System.out.println("Intruducir un número: ");
        numero = sc.nextInt();
        sc.close();

        System.out.println("----------------------------------");
        System.out.println("Has introducido el número "+numero+" gracias!!!");
    }
}
