import java.util.Scanner;

public class ejercicio6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        float base, altura, area, perimetro;

        System.out.println("Introduce el valor de la base del rectángulo: ");
        base = sc.nextFloat();

        System.out.println("Introduce el valor de la altura del rectángulo: ");
        altura = sc.nextFloat();
        sc.close();

        area = base*altura;
        perimetro = (2*base)+(2*altura);

        System.out.println("-----------------------Fórmulas------------------------");
        System.out.println("Perímetro = 2 veces la base + 2 veces la altura");
        System.out.println("Área = base x altura");
        System.out.println("-----------------------------------------------");
        System.out.println("");
        System.out.println("");
        System.out.println("-----------------------Resultado------------------------");
        System.out.println("Perímetro = "+perimetro);
        System.out.println("Área = "+area);
        System.out.println("-----------------------------------------------");


    }
}
