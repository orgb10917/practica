import java.util.Scanner;

public class ejercicio5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        float precio, descuento, total;

        System.out.println("Precio del artículo: ");
        precio = sc.nextFloat();

        System.out.println("Descuento del artículo: ");
        descuento = sc.nextFloat();
        sc.close();

        total = precio * (descuento/100);

        System.out.println("----------------------------------------------");
        System.out.println("El artículo tiene un costo de: "+precio);
        System.out.println("Descuento: "+descuento+"%");
        System.out.println("Total= "+total);
        System.out.println("----------------------------------------------");
    }
}
