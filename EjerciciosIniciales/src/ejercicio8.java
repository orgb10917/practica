import java.util.Scanner;

public class ejercicio8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double pi = 3.1416;
        double radio, area, volumen;

        System.out.println("Introducir el radio de la circunferencia: ");
        radio = sc.nextDouble();
        sc.close();

        area = pi * Math.pow(radio,2);

        volumen = (4*pi*Math.pow(radio,3))/3;

        System.out.println("---------------------------------------------");
        System.out.println("Área: "+area);
        System.out.println("Volumen de la esfera: "+volumen);
        System.out.println("---------------------------------------------");

    }
}
