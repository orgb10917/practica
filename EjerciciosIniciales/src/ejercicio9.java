import java.util.*;
public class ejercicio9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a,b,c;
        double x1, x2;

        System.out.println("Ecuación de la forma: ax^2+bx+c");

        System.out.println("Digite el valor de la variable a: ");
        a = sc.nextInt();

        System.out.println("Digite el valor de la variable b: ");
        b = sc.nextInt();

        System.out.println("Digite el valor de la variable c: ");
        c = sc.nextInt();

        x1 = (-b+(Math.sqrt(Math.pow(b,2)-(4*a*c))))/2;
        x2 = (-b-(Math.sqrt(Math.pow(b,2)-(4*a*c))))/2;

        System.out.println("-------------------------------------------------------------");
        System.out.println("Su ecuación es la siguiente: "+a+"x^2+"+b+"x+"+c);

        System.out.println("<<<<<Soluciones>>>>>");
        System.out.println("x1= "+x1);
        System.out.println("x2= "+x2);
        System.out.println("-------------------------------------------------------------");
    }
}
