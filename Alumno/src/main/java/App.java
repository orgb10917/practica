import java.util.Scanner;
import Pojo.Alumnos;

public class App {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws InterruptedException {
        boolean flag = true;
        int opc;
        int estudiantes;
        System.out.println("Ingrese la cantidad de estudiante: ");
        estudiantes = sc.nextInt();

        Alumnos []alumno = new Alumnos[estudiantes];
        int contador  = 0;

        do {
            menu();
            opc = sc.nextInt();

            switch (opc){
                case 1:
                    if (contador<estudiantes){
                        registrar_estudiante(contador, alumno);
                        contador ++;
                    }else{
                        System.out.println("Ha llegado al límite");
                        break;
                    }
                    break;

                case 2:
                    busqueda_nombre(contador, alumno);
                    break;

                case 3:
                    ver_listado(contador, alumno);
                    break;

                case 4:
                    modificar_datos(contador, alumno);
                    break;

                case 5:
                    System.out.println("Saliendo....");
                    Thread.sleep(3000);
                    flag = false;
                    break;

                default:
                    System.out.println("Ingrese una opción correcta");
                    break;
            }
        }while (flag);
    }

    public static void modificar_datos(int contador, Alumnos[] alumno) throws InterruptedException {
        String nombre;
        String apellido;
        int edad;

        String nombre_buscar;
        sc.nextLine();
        System.out.println("Ingrese el nombre: ");
        nombre_buscar = sc.nextLine();

        for (int i = 0; i<contador; i++){
            if (alumno[i].getNombre().equalsIgnoreCase(nombre_buscar)){
                sc.nextLine();
                System.out.println("Ingrese el nuevo nombre: ");
                nombre = sc.nextLine();

                System.out.println("Ingrese los nuevos apellidos: ");
                apellido = sc.nextLine();

                System.out.println("Ingrese la nueva edad: ");
                edad = sc.nextInt();

                System.out.println("Actualizando el dato del alumno....");
                Thread.sleep(3000);

                alumno[i] = new Alumnos(nombre, apellido, edad);
                return;
            }
        }
    }

    public static void ver_listado(int contador, Alumnos[] alumno) {
        System.out.println("------------------------Registro---------------------------");
        for (int i = 0; i<contador; i++){
            System.out.println("Nombre: "+alumno[i].getNombre()+" "+alumno[i].getApellido());
            System.out.println("Edad: "+alumno[i].getEdad());
            System.out.println("------------------------------------------------------------");
        }
    }

    public static void registrar_estudiante(int contador, Alumnos[] alumnos) {
        String nombre;
        String apellido;
        int edad;

        sc.nextLine();
        System.out.println("Ingrese el nombre: ");
        nombre = sc.nextLine();

        System.out.println("Ingrese el apellido: ");
        apellido = sc.nextLine();

        do {
            System.out.println("Ingrese la edad: ");
            edad = sc.nextInt();
        }while (edad<0);

        alumnos[contador] = new Alumnos(nombre, apellido, edad);
    }

    public static void busqueda_nombre(int contador, Alumnos[] alumnos){
        String nombre_buscar;
        sc.nextLine();
        System.out.println("Ingrese el nombre: ");
        nombre_buscar = sc.nextLine();

        for (int i = 0; i<=contador; i++){
            if (alumnos[i].getNombre().equalsIgnoreCase(nombre_buscar)){
                System.out.println("Nombre: "+alumnos[i].getNombre()+" "+alumnos[i].getApellido());
                System.out.println("Edad: "+alumnos[i].getEdad());
                return;
            }
        }
    }

    public static void menu(){
        System.out.println("1. Registrar estudiante");
        System.out.println("2. Buscar por nombre");
        System.out.println("3. Ver listado de alumnos");
        System.out.println("4. Modificar datos del alumno");
        System.out.println("5. Salir");
    }
}
