/*
1. Pide el número de auto
2. Pide el número de millas recorridas (Si las millas recorridas
    son mayores a 80 que es él limite, indicar un mensaje "Esta
    arriba del límite de velocidad".)
3. Pide los kilómetros recorridos del auto (Si los kilómetros
    recorridos son mayores a 200 y menores a 350 entonces
    indicar "Hace falta mantenimiento al auto".)
4. Pide los kilómetros que recorre un auto con un litro de
    gasolina. De 10 a 16.
5. Si el auto recorre máximo 16 kilómetros por litro y mínimo 14
    kilómetros por litro, entonces desplegar un mensaje "Consume
    poca gasolina" . Y calcular cuántos litros de gasolina consume
    un auto que recorre 100 kilómetros.
6. Si el auto recorre máximo 13 kilómetros por litro, y mínimo 10
    kilómetros por litro, entonces desplegar un mensaje "Consume
    algo de gasolina".
 */
import java.util.Scanner;
public class Ejercicio3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num_auto;
        float num_millas;
        float num_kilometros;
        float kilometros_recorre;

        System.out.println("Digite el número del auto: ");
        num_auto = sc.nextInt();

        System.out.println("Digite el número de millas recorridas: ");
        num_millas = sc.nextFloat();

        System.out.println("Digite el número de kilómetros recorridos: ");
        num_kilometros = sc.nextFloat();

        do {
            System.out.println("Digite el número de kilometros que recorre con 1 litro de gasolina entre 10 km y 16 km");
            kilometros_recorre = sc.nextFloat();

        }while(kilometros_recorre<10||kilometros_recorre>16);

        System.out.println("-----------------------------Registro---------------------------------------");
        System.out.println("Número del auto: "+num_auto);
        System.out.println("Número de millas recorridas: "+num_millas);
        System.out.println("Número de kilómetros recorridos: "+num_kilometros);
        System.out.println("Recorre: "+kilometros_recorre+"km por galón");
        System.out.println("--------------------------------------------------------------------");

        System.out.println("-----------------------------Observaciones---------------------------------------");
        if (num_millas>80){
            System.out.println("Número de millas mayor a 80, está arriba del límite de velocidad");
        }
        if (num_kilometros>200 && num_kilometros<350){
            System.out.println("Hace falta mantenimiento al auto");
        }
        if (kilometros_recorre>=14 && kilometros_recorre<=16){
            float gasolina;
            System.out.println("Consume poca gasolina");
            gasolina = 100/kilometros_recorre;
            System.out.println("Por 100 km comsume: "+gasolina+" litros de gasolina");
            System.out.println("--------------------------------------------------------------------");
        }
        if (kilometros_recorre >=10 && kilometros_recorre<=13){
            System.out.println("Consume algo de gasolina");
            System.out.println("--------------------------------------------------------------------");
        }
    }
}
