/*
Elabora un programa para el equipo de Basket Ball, el cual requiere saber por
medio de sistema cuantos puntos anota cada uno de sus jugadores y otros puntos:
    1. Pide el número de jugador
    2. Pide cuántos tiros anotó el jugador
    3. Pide cuántos tiros falló el jugador (Si falló al menos un tiro y
        anotó al menos un tiro, calcular cuantos tiros en total tuvo el
        jugador, anotados o no anotados.)
    4. Pide cuántos puntos anotó el jugador
    5. Si los puntos anotados son más de 6 y menos
        de 3 entonces indicar "Anotó pocos puntos".
    6. Si los puntos anotados son más de 15 y
        menos de 7 entonces indicar "Anotó puntos
        aceptables". Y además calcular cuántos
        puntos de tres en promedio pudo haber
        anotado".
    7. Si los puntos anotados son más de 22 y
        menos de 16 entonces indicar "Felicidades
        por sus anotaciones". Y además calcular
        cuántos puntos de tres en promedio pudo
        haber anotado".
 */
import java.util.Scanner;
public class Ejercicio4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int numero_jugador;
        int tiros_anotados;
        int tiros_fallados;
        int tiros_totales;
        int puntos;

        System.out.println("Ingrese el número del jugador: ");
        numero_jugador = sc.nextInt();

        System.out.println("Ingrese el número de tiros anotados: ");
        tiros_anotados = sc.nextInt();

        System.out.println("Ingrese el número de tiros fallados: ");
        tiros_fallados = sc.nextInt();

        System.out.println("Ingrese el número de puntos del jugador: ");
        puntos = sc.nextInt();

        tiros_totales =  tiros_fallados + tiros_anotados;

        System.out.println("-----------------------------Registro---------------------------------------");

        if (puntos>=3 && puntos<=6){
            System.out.println("Número de jugador: "+numero_jugador);
            System.out.println("Tiros totales: "+tiros_totales);
            System.out.println("Tiros anotados: "+tiros_anotados);
            System.out.println("Tiros fallados: "+tiros_fallados);
            System.out.println("Número de puntos: "+puntos);
            System.out.println("Anotó pocos puntos");

        }else if(puntos>=7 && puntos<=15){
            int puntos_tres;
            System.out.println("Número de jugador: "+numero_jugador);
            System.out.println("Tiros totales: "+tiros_totales);
            System.out.println("Tiros anotados: "+tiros_anotados);
            System.out.println("Tiros fallados: "+tiros_fallados);
            System.out.println("Número de puntos: "+puntos);
            System.out.println("Anotó puntos aceptables");
            puntos_tres = puntos/3;
            System.out.println("Anotó un promedio de: "+puntos_tres+" puntos de 3");
        }else if(puntos>=16 && puntos<=22){
            int puntos_tres;
            System.out.println("Número de jugador: "+numero_jugador);
            System.out.println("Tiros totales: "+tiros_totales);
            System.out.println("Tiros anotados: "+tiros_anotados);
            System.out.println("Tiros fallados: "+tiros_fallados);
            System.out.println("Número de puntos: "+puntos);
            System.out.println("Felicidades por sus anotaciones");
            puntos_tres = puntos/3;
            System.out.println("Anotó un promedio de: "+puntos_tres+" puntos de 3");
        }
        System.out.println("--------------------------------------------------------------------");

    }
}
