/*
- Elabora el siguiente programa, en el cual pidas al usuario:
    Día de nacimiento
    Mes de nacimiento
    Año de nacimiento
    Edad (Si la edad del usuario es mayor a 18 años,entonces muestra el mensaje: "Podrás formar parte del equipo de (algún deporte)".
Además pide:
    Grado escolar que cursa
    Grupo
    Promedio hasta el último semestre. (Si el promedio es menor a 70, favor de mostrar un mensaje de: "Sentimos mucho pero no podrás formar parte por tu bajo promedio".)
    Horario que prefiere de entrenamiento
 */
import java.util.Scanner;
public class Ejercicio2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //VARIABLES DE NACIMIENTO:
        String nombre;
        int dia;
        int mes;
        int anio;
        int edad;
        //VARIABLES ESCOLARES:
        int grado;
        String grupo;
        float  promedio;

        System.out.println("Digite: ");
        System.out.println("Nombre: ");
        nombre = sc.nextLine();
        System.out.println("-Día de nacimiento: ");
        dia = sc.nextInt();
        do {
            System.out.println("-Mes de nacimiento: ");
            mes = sc.nextInt();
        }while (mes<0 || mes>13);
        System.out.println("-Año de nacimiento: ");
        anio = sc.nextInt();
        System.out.println("-Edad actual: ");
        edad = sc.nextInt();
        System.out.println("-Grado en que cursa: ");
        grado = sc.nextInt();
        sc.nextLine();
        System.out.println("-Grupo: ");
        grupo = sc.nextLine();
        System.out.println("-Promedio del último semestre: ");
        promedio = sc.nextFloat();

        if(edad >=18){
            int opc;

            System.out.println("Puedes formar parte de un equipo!!!");
            do {
                System.out.println("Escoge: ");
                System.out.println("1. Basketball");
                System.out.println("2. Football");
                System.out.println("3. Volleyball");
                System.out.println("4. Baseball");
                opc = sc.nextInt();
                switch (opc) {
                    case 1 -> {
                        int opc_entrenamiento;
                        System.out.println("Bienvenido al equipo de Basketball");
                        do {
                            System.out.println("Escoge el horario de entrenamiento: ");
                            System.out.println("1. Matutino 6:00 am - 7:00 am");
                            System.out.println("2. Nocturno 6:00 pm - 7:00 pm");
                            opc_entrenamiento = sc.nextInt();
                        } while (opc_entrenamiento < 0 || opc_entrenamiento > 2);
                        System.out.println("-----------------------------Registro---------------------------------------");
                        System.out.println("Nombre: " + nombre);
                        System.out.println("Fecha nacimiento: " + dia + "/" + mes + "/" + anio);
                        System.out.println("Edad: " + edad);
                        System.out.println("Grado: " + grado + "       Grupo: " + grupo);
                        System.out.println("Promedio: " + promedio);
                        System.out.println("Equipo: Basketball");
                        if (opc_entrenamiento == 1) {
                            System.out.println("Horario de entrenamiento: Matutino 6:00 am - 7:00 am");
                        } else {
                            System.out.println("Horario de entrenamiento: Nocturno 6:00 pm - 7:00 pm");
                        }
                        System.out.println("--------------------------------------------------------------------");
                    }
                    case 2 -> {
                        int opc_entrenamiento;
                        System.out.println("Bienvenido al equipo de Football");
                        do {
                            System.out.println("Escoge el horario de entrenamiento: ");
                            System.out.println("1. Matutino 6:00 am - 7:00 am");
                            System.out.println("2. Nocturno 6:00 pm - 7:00 pm");
                            opc_entrenamiento = sc.nextInt();
                        } while (opc_entrenamiento < 0 || opc_entrenamiento > 2);
                        System.out.println("-----------------------------Registro---------------------------------------");
                        System.out.println("Nombre: " + nombre);
                        System.out.println("Fecha nacimiento: " + dia + "/" + mes + "/" + anio);
                        System.out.println("Edad: " + edad);
                        System.out.println("Grado: " + grado + "       Grupo: " + grupo);
                        System.out.println("Promedio: " + promedio);
                        System.out.println("Equipo: Football");
                        if (opc_entrenamiento == 1) {
                            System.out.println("Horario de entrenamiento: Matutino 6:00 am - 7:00 am");
                        } else {
                            System.out.println("Horario de entrenamiento: Nocturno 6:00 pm - 7:00 pm");
                        }
                        System.out.println("--------------------------------------------------------------------");
                    }
                    case 3 -> {
                        int opc_entrenamiento;
                        System.out.println("Bienvenido al equipo de Volleyball");
                        do {
                            System.out.println("Escoge el horario de entrenamiento: ");
                            System.out.println("1. Matutino 6:00 am - 7:00 am");
                            System.out.println("2. Nocturno 6:00 pm - 7:00 pm");
                            opc_entrenamiento = sc.nextInt();
                        } while (opc_entrenamiento < 0 || opc_entrenamiento > 2);
                        System.out.println("-----------------------------Registro---------------------------------------");
                        System.out.println("Nombre: " + nombre);
                        System.out.println("Fecha nacimiento: " + dia + "/" + mes + "/" + anio);
                        System.out.println("Edad: " + edad);
                        System.out.println("Grado: " + grado + "       Grupo: " + grupo);
                        System.out.println("Promedio: " + promedio);
                        System.out.println("Equipo: Volleyball");
                        if (opc_entrenamiento == 1) {
                            System.out.println("Horario de entrenamiento: Matutino 6:00 am - 7:00 am");
                        } else {
                            System.out.println("Horario de entrenamiento: Nocturno 6:00 pm - 7:00 pm");
                        }
                        System.out.println("--------------------------------------------------------------------");
                    }
                    case 4 -> {
                        int opc_entrenamiento;
                        System.out.println("Bienvenido al equipo de Baseball");
                        do {
                            System.out.println("Escoge el horario de entrenamiento: ");
                            System.out.println("1. Matutino 6:00 am - 7:00 am");
                            System.out.println("2. Nocturno 6:00 pm - 7:00 pm");
                            opc_entrenamiento = sc.nextInt();
                        } while (opc_entrenamiento < 0 || opc_entrenamiento > 2);
                        System.out.println("-----------------------------Registro---------------------------------------");
                        System.out.println("Nombre: " + nombre);
                        System.out.println("Fecha nacimiento: " + dia + "/" + mes + "/" + anio);
                        System.out.println("Edad: " + edad);
                        System.out.println("Grado: " + grado + "       Grupo: " + grupo);
                        System.out.println("Promedio: " + promedio);
                        System.out.println("Equipo: Baseball");
                        if (opc_entrenamiento == 1) {
                            System.out.println("Horario de entrenamiento: Matutino 6:00 am - 7:00 am");
                        } else {
                            System.out.println("Horario de entrenamiento: Nocturno 6:00 pm - 7:00 pm");
                        }
                        System.out.println("--------------------------------------------------------------------");
                    }
                    default -> System.out.println("Sorry digita el número correcto");
                }
            }while(opc<1||opc>4);

        }else{
            System.out.println("-----------------------------Registro---------------------------------------");
            System.out.println("Nombre: " + nombre);
            System.out.println("Fecha nacimiento: " + dia + "/" + mes + "/" + anio);
            System.out.println("Edad: " + edad);
            System.out.println("Grado: " + grado + "       Grupo: " + grupo);
            System.out.println("Promedio: " + promedio);
            System.out.println("--------------------------------------------------------------------");
        }

    }
}
