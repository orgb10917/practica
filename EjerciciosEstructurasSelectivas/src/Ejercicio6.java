/*
Elabora un pequeño programa para clasificar los productos del almacén de un
supermercado que entran día a día.

Deberás pedir la clasificación del producto y:

    Si la clasificación        Indicar que es de:
        dada es:
            F                     Frutas
            V                     Verduras
            S                     Salchichonería
            L                     Lácteos
            B                     Bebés
            D                     Damas
            C                     Caballeros
            N                     Niños
            P                     Perfumería
            M                     Medicamento farmacéutico
            E                     Electrónicos
            H                     Hogar
            J                     Juguetería

De no coincidir con ninguna de las clasificaciones antes mencionadas indicar: "Producto
inválido".

Además: Si el producto tiene clasificación "S" indicar "Tendrá un 15 porciento de
descuento todos los fines de semana".

Si el producto tiene clasificación "F", "V", indicar "Tendrá un 20 porciento de descuento
todos los martes".
 */

import java.util.Scanner;
public class Ejercicio6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String nombre;
        String producto;
        String clasificacion;

        System.out.println("Ingrese el nombre del producto: ");
        nombre = sc.nextLine();

        System.out.println("Indica la clasificación en mayúscula según la letra que le corresponda: ");
        System.out.println("\n" +
                "            F                     Frutas\n" +
                "            V                     Verduras\n" +
                "            S                     Salchichonería\n" +
                "            L                     Lácteos\n" +
                "            B                     Bebés\n" +
                "            D                     Damas\n" +
                "            C                     Caballeros\n" +
                "            N                     Niños\n" +
                "            P                     Perfumería\n" +
                "            M                     Medicamento farmacéutico\n" +
                "            E                     Electrónicos\n" +
                "            H                     Hogar\n" +
                "            J                     Juguetería");

        clasificacion = sc.next();
        String clasificacion_s = "S";
        String clasificacion_f = "F";
        String clasificacion_v = "V";

        if (clasificacion.equalsIgnoreCase(clasificacion_s)){
            System.out.println("Nombre del producto: "+nombre);
            System.out.println("Clasificación: "+clasificacion);
            System.out.println("Tendra un 15% de descuento en todos los fines de semana");
        }else if (clasificacion.equalsIgnoreCase(clasificacion_f) || clasificacion.equalsIgnoreCase(clasificacion_v)){
            System.out.println("Nombre del producto: "+nombre);
            System.out.println("Clasificación: "+clasificacion);
            System.out.println("Tendra un 20% de descuento todos los martes");
        }else{
            System.out.println("Nombre del producto: "+nombre);
            System.out.println("Clasificación: "+clasificacion);
        }
    }
}
