/*
    1- Elabora el siguiente programa el cual pidas lo siguiente:
                - Código de barras del artículo
                - Cantidad de artículos
                - Si la cantidad de artículos es cuatro únicamente se te cobraran tres.
                - Costo del artículo
        Posteriormente calcula el total a pagar, el cual dependerá de la cantidad de
        artículos a pagar y el costo de los mismos.
        Si el total a pagar es mayor de 300 pesos despliega un mensaje en pantalla de: por
        cada 100 pesos de compra se te obsequiarán 10 pesos en vales.
     */
import java.util.Scanner;
public class Ejercicio1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int codigo_barras;
        int cantidad_articulos;
        float costo_articulos;
        float total_pagar;

        System.out.println("Digite el código de barras: ");
        codigo_barras = sc.nextInt();

        System.out.println("Cantidad de artículos: ");
        cantidad_articulos = sc.nextInt();

        System.out.println("Digite el costo del artículo: ");
        costo_articulos = sc.nextFloat();

        if(cantidad_articulos == 4){
            total_pagar = 3 * costo_articulos;
            if (total_pagar >= 300){
                float calculo_vales;
                System.out.println("Código del producto: "+codigo_barras);
                System.out.println("Cantidad: "+cantidad_articulos);
                System.out.println("Total: C$ "+total_pagar);
                calculo_vales = total_pagar/100;
                int total_vales = (int) calculo_vales;
                System.out.println("Por cada C$100 de compra, se te obsequiará C$10 en vales!!!!");
                System.out.println("Total de vales: C$"+total_vales);
            }else{
                System.out.println("Código del producto: "+codigo_barras);
                System.out.println("Cantidad: "+cantidad_articulos);
                System.out.println("Total: C$ "+total_pagar);
            }
        }else{
            total_pagar = cantidad_articulos * costo_articulos;
            if (total_pagar >= 300){
                float calculo_vales;
                System.out.println("Código del producto: "+codigo_barras);
                System.out.println("Cantidad: "+cantidad_articulos);
                System.out.println("Total: C$ "+total_pagar);
                calculo_vales = total_pagar/100;
                int total_vales = (int) calculo_vales;
                System.out.println("Por cada C$100 de compra, se te obsequiará C$10 en vales!!!!");
                System.out.println("Total de vales: C$"+total_vales);
            }else{
                System.out.println("Código del producto: "+codigo_barras);
                System.out.println("Cantidad: "+cantidad_articulos);
                System.out.println("Total: C$ "+total_pagar);
            }
        }
    }
}
