/*
Elabora un pequeño programa en el cual pidas:

    1. Código del producto.
    2. Año de elaboración de un producto.
    3. Mes de caducidad.
    4. Año de caducidad.

Si el producto tiene código aceptable máximo de 150 y mínimo de 80 y su año de
elaboración es igual a 2016 y además el mes de caducidad es menor al mes 9
(septiembre) y también el año de caducidad es igual al 2020. Entonces ese producto será
aceptable, indicando con mensaje: "el sistema le asignará un lugar en el
almacén". Posteriormente deberás calcular cuántos años tiene aun de vigencia el
producto y mostrar en pantalla.

De lo contrario el mensaje indicará: "El sistema no puede asignar un lugar en el
almacén debido a sus características dadas".
 */
import java.util.Scanner;
public class Ejercicio5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int codigo_producto;
        int anio_elaboracion;
        int mes_caducidad;
        int anio_caducidad;

        System.out.println("Ingrese el código del producto: ");
        codigo_producto = sc.nextInt();

        System.out.println("Ingrese el año en que se elaboró el producto: ");
        anio_elaboracion = sc.nextInt();

        do {
            System.out.println("Ingrese el mes de caducidad del producto: ");
            mes_caducidad = sc.nextInt();
        }while(mes_caducidad<1 || mes_caducidad>12);

        System.out.println("Ingrese el año de caducidad del producto: ");
        anio_caducidad = sc.nextInt();

        if (codigo_producto > 79 && codigo_producto < 151) {
            if (anio_elaboracion == 2016) {
                if (mes_caducidad == 9) {
                    if (anio_caducidad == 2020) {
                        int tiempo_caducidad;
                        tiempo_caducidad = anio_caducidad - anio_elaboracion;
                        System.out.println(">>>>>El sistema le asignará un lugar en el almacén<<<<<");
                        System.out.println("Código del producto: " + codigo_producto);
                        System.out.println("Año de elaboración: " + anio_elaboracion);
                        System.out.println("Fecha de caducidad: " + mes_caducidad + "/" + anio_caducidad);
                        System.out.println("Le queda un tiempo de: " + tiempo_caducidad + " para caducar");
                    }
                }else {
                    System.out.println("El sistema no le puede asignar un lugar en el almacén debido a sus características dadas");
                }
            }else {
                System.out.println("El sistema no le puede asignar un lugar en el almacén debido a sus características dadas");
            }
        } else {
            System.out.println("El sistema no le puede asignar un lugar en el almacén debido a sus características dadas");
        }
    }
}
